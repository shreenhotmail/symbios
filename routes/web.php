<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>["web","admin"]],function() {

    Route::resource('controlpanel/subCategory', 'CategoryController');
    Route::resource('controlpanel/question', 'QuestionsController');
    Route::get('controlpanel/category', 'CategoryController@mainCat');
    Route::get('controlpanel/category/{catId}/edit', 'CategoryController@showMainCat');
    Route::POST('controlpanel/updateMain/{id}', 'CategoryController@updateMain');
    Route::get('/controlpanel', 'DashBoardController@dashboard');
});


/*front end*/
Route::group(['middleware'=>"web"],function(){
    Route::auth();
    Route::get('/', 'HomeController@index');
    Route::get('/questions/{slag}', 'HomeController@questions');
    Route::post('/questionResult/{slag}', 'HomeController@questionResult');
    Route::get('/sections/{slag}', 'HomeController@sections');
});
Route::get('/',function(){
    return view('home');
});Route::get('/dashboard',function(){
    return view('results');
});
Route::get('/home',function(){
    return view('home');
});
