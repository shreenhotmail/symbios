<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    public $table = 'main_categories';
    public $timestamps = false;
}
