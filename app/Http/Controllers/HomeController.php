<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function questions($slag)
    {
        $slag = str_replace('_', ' ', $slag);
        $slag = str_replace('-', '/', $slag);
        $sections = Category::where('cat_name', '=', $slag)->get();
        $hasControls = false;
        if (isset($sections[0])) {
            $questions = Question::where('cat_id', '=', $sections[0]->id)->get();
            sizeof($questions) > 1 ? $hasControls = true : $hasControls = false;
            foreach ($questions as $k => $q) {
                $questions[$k]->answers = DB::table('answers')->where('question_id', '=', $q->id)->get();
            }
            $sections = $sections[0];
        } else {
            $questions = [];
            $sections = [];
        }
        $slag = str_replace('_', ' ', $slag);
        $key = str_replace('/', '-', $slag);

        return view('questions', compact('questions', 'key', 'sections' , 'hasControls'));
    }

    public function questionResult($slag, Request $request)
    {
        $slag = str_replace('_', ' ', $slag);
        $slag = str_replace('-', '/', $slag);
        $sections = Category::where('cat_name', '=', $slag)->get();
        
        if (isset($sections[0])) {
            $questions = Question::where('cat_id', '=', $sections[0]->id)->get();
            $result = 0;
        //    dd(($questions));
            foreach ($questions as $k => $q) {
                $answers = DB::table('answers')->where('question_id', '=', $q->id)->get();
                // dd(($request));
                $id = $q->id;
                $name = 'optradio_' . $id;
                foreach($answers as $oneAnswer){
                    if ($request->$name == $oneAnswer->id) {
                        $result+= $oneAnswer->rate;
                    }
                }
                
            }
            $finalResult = (($result * 100)/100);
            // dd($finalResult);
            $getUsersResult = DB::table('results')->where('user_id', '=', Auth::id())
                ->where('main_cat_id', '=', $sections[0]->main_cat)
                ->where('sub_cat_id', '=', $sections[0]->id)->count();
            if ($getUsersResult == 0) {
                $insertResult = DB::table('results')->insert([
                    "user_id" => Auth::id(),
                    "main_cat_id" => $sections[0]->main_cat,
                    "sub_cat_id" => $sections[0]->id,
                    "result" => $finalResult
                ]);
            } else {
                $insertResult = DB::table('results')->where('user_id', '=', Auth::id())
                    ->where('main_cat_id', '=', $sections[0]->main_cat)
                    ->where('sub_cat_id', '=', $sections[0]->id)->update([

                        "result" => $finalResult
                    ]);
            }


            $getUsersResult = DB::table('main_categories')->leftJoin('results', function ($leftJoin) {
                $leftJoin->on('main_categories.id', '=', 'results.main_cat_id')
                    ->where('results.user_id', '=', Auth::id());

            })->select(DB::raw('avg(results.result) as avg'))->groupBy('main_categories.id')->get();

            $data = [];
            foreach ($getUsersResult as $result) {

                $data[] = $result->avg ? (float)$result->avg : 0;
            }
//var_dump(Auth::user)
            session()->put('result', $finalResult);
            session()->put('data', $data);
            session()->put('slag', $slag);
            return redirect('/dashboard');
//            }

        }

    }

    public function sections($slag)
    {
        $slag = str_replace('-', ' ', $slag);

        $mainSections = DB::table('main_categories')->where('category_name', '=', $slag)->get();
        if (isset($mainSections[0])) {
            $image = $mainSections[0]->image;
            $sections = Category::where('main_cat', '=', $mainSections[0]->id)->get();
        } else {
            $sections = [];
            $image = '';
        }
        $key = str_replace('_', ' ', $slag);
        return view('sections', compact('sections', 'key', 'image'));
    }
}
