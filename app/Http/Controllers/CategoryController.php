<?php

namespace App\Http\Controllers;

use App\Category;
use App\MainCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::all();

        return view('admin.category.categories', compact('categories'));
    }

    public function edit($id)
    {
        $cat = Category::find($id);
        return view('admin.category.edit', compact('cat'));
    }

    public function mainCat()
    {
//        $cat = DB::table('main_categories')->find($id);
        $success = [];
        return view('admin.category.mainCat', compact('success'));
    }

    public function show($id)
    {
        $cat = Category::find($id)->get();
        return view('admin.category.edit', compact('cat'));

    }

    public function showMainCat($catId){
        $mainCategory = MainCategory::find($catId);
        return view('admin.category.editMainCat', compact('mainCategory'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
//        var_dump('ss');die;
        $rules = [];
        $rules['cat_name'] =  'required';
        $image = $request->file('image');
        if (!empty($image)) {
            $rules['image'] =  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $image = $request->file('image');
            if (isset($image)) {

                    $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();

                    $destinationPath = public_path('/images');

                    $image->move($destinationPath, $input['imagename']);
                    $image = $input['imagename'];
                }



            $url = $request->link;
            if (!empty($url)) {

                if ($url) {
                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                    $video_id = $match[1];
                }
            } else {
                $video_id = '';
            }
            $question = Category::create([
                'cat_name' => $request->cat_name,
                'link' => $video_id,
                'main_cat' => $request->main_cat,
                'image' => $image ? $image : '',

            ]);
            return redirect('/controlpanel/subCategory/');

        }
        $errors = $validator->errors()->all();
        if (empty($errors)) {
            return redirect('/controlpanel/subCategory/');
        } else {
            return view('admin.category.create', compact('errors'));
        }
//        if ($validator->fails()) {
//            return redirect('/controlpanel/subCategory/create')
//                ->withErrors($validator)
//                ->withInput();
//        }

    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cat_name' => 'required',
//            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ])->setAttributeNames(['answer[]' => 'Question Answers ']);

        if ($validator->passes()) {

            $image = $request->file('image');
            $url = $request->link;
            if (!empty($url)) {

                if ($url) {
                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                    $video_id = $match[1];
                }
            } else {
                $video_id = '';
            }
            $question = Category::where('id', '=', $id)->update([
                'cat_name' => $request->cat_name,
                'link' => $video_id,
                'main_cat' => $request->main_cat,

            ]);
            if (isset($image)) {
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();

                $destinationPath = public_path('/images');

                $image->move($destinationPath, $input['imagename']);
                $question = Category::where('id', '=', $id)->update([

                    'image' => $input['imagename'],

                ]);
            }
        }
        $errors = $validator->errors()->all();
        if (empty($errors)) {
            return redirect('/controlpanel/subCategory/');
        } else {

            return view('admin.category.create', compact('errors'));
        }

    }

    public function updateMain($id ,Request $request)
    {
        $mainCategory = MainCategory::find($id);
        if(!$mainCategory->image || $request->file('image')){
            $validator = Validator::make($request->all(), [
                'main_cat' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
        }
        else
        {
            $validator = Validator::make($request->all(), [
                'main_cat' => 'required'    
            ]);
        }

        // dd($validator->errors()->all());

        if ($validator->passes()) {
            $image = $request->file('image');
            if($image){
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $input['imagename']);
                $mainCat = DB::table('main_categories')->where('id', '=', $id)->update(['image' => $input['imagename'] , 'category_name' =>$request->main_cat]);
            }
            else
            {
                $mainCat = DB::table('main_categories')->where('id', '=', $id)->update(['category_name' =>$request->main_cat]);

            }
            $errors = $validator->errors()->all();
            if (empty($errors)) {
                $success = 'Your Image Added Successfully';
                Session::flash('message', $success);
                return redirect('/controlpanel/category/');
            }

        }else {
            $errors = $validator->errors()->all();
            
            return view('admin.category.editMainCat', compact('mainCategory' , 'errors'));
        }
//        if ($validator->fails()) {
//            return redirect('/controlpanel/category/')
//                ->withErrors($validator)
//                ->withInput();
//        }

    }

    public function destroy($id)
    {

        $delete = Category::destroy($id);
        return redirect('/controlpanel/subCategory/');
    }

}
