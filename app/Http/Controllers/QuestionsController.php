<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class QuestionsController extends Controller
{

    public function index()
    {
//    $allQuestions=Question::all();
        $allQuestions = DB::table('questions')->join('categories', 'questions.cat_id', '=', 'categories.id')
            ->select('questions.id as questionId', 'questions.*', 'cat_name')->get();

        return view('admin.question.questions', compact('allQuestions'));
    }

    public function edit($id)
    {
        $question = DB::table('questions')
            ->join('categories', 'questions.cat_id', '=', 'categories.id')
            ->select('questions.id as questionId', 'questions.*', 'categories.*')
            ->where('questions.id', '=', $id)->get();
        foreach ($question as $k => $answer) {
            $question[$k]->answer = Answer::where('question_id', '=', $id)->get();
        }
        return view('admin.question.edit', compact('question'));
    }


    public function show($id)
    {
        $question = DB::table('questions')
            ->join('categories', 'questions.cat_id', '=', 'categories.id')
            ->select('questions.id as questionId', 'questions.*', 'categories.*')
            ->where('questions.id', '=', $id)->get();
        foreach ($question as $k => $answer) {
            $question[$k]->answer = Answer::where('question_id', '=', $id)->get();
        }
        return view('admin.question.edit', compact('question'));
    }

    public function create()
    {
        return view('admin.question.create');
    }

    public function store(Request $request)
    {
        // dd($request->answerRate[0]);
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'cat' => 'required',
            'question' => 'required',
        ])->setAttributeNames(['answer[]' => 'Question Answers '])
        ->setAttributeNames(['rate[]' => 'Answer Rate ']);

        if ($validator->passes()) {
            $question = Question::create([
                'cat_id' => $request->cat,
                'question' => $request->question
            ]);
            $answerRateIndex = 0;
            foreach ($request->answer as $answer) {
            
                $answer = Answer::create([
                    'question_id' =>$question->id,
                    'answer' => $answer,
                    'rate' => $request->answerRate[$answerRateIndex]
                ]);
                $answerRateIndex++;
            }
        }
        $errors = $validator->errors()->all();
        if(empty($errors)){
            return  redirect('/controlpanel/question/');
        }else{

        return view('admin.question.create', compact('errors'));
        }

    }


    public function update($id,Request $request){
        $validator = Validator::make($request->all(), [
            'cat' => 'required',
            'question' => 'required',

        ]);

        if ($validator->passes()) {
            $question = Question::where('id','=',$id)->update([
                'cat_id' => $request->cat,
                'question' => $request->question
            ]);
            $answerRateIndex = 0;
            foreach ($request->answer as $index => $answer) {
                
                if(isset($request->answer_id[$index])){

                $answerUpdate = Answer::where('id','=',$request->answer_id[$index])->update([
                    'question_id' => $id,
                    'answer' => $answer,
                    'rate' => $request->answerRate[$answerRateIndex]
                ]);
                
                }else{
                    $answer = Answer::create([
                        'question_id' => $id,
                        'answer' => $answer,
                        'rate' => $request->answerRate[$answerRateIndex]
                    ]);
                }
                $answerRateIndex++;
            }

        }
        $errors = $validator->errors()->all();
        if(empty($errors)){
            return  redirect('/controlpanel/question/');
        }else{
            return view($this->edit($request->question), compact('errors'));
        }

    }


    public function destroy($id)
    {
        $delete = Question::destroy($id);
        $answers=Answer::where('question_id','=',$id)->delete();
        return  redirect('/controlpanel/question/');
    }

}


