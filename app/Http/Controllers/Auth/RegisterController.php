<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'industry' => 'required|string|max:255',
            'years_of_experience' => 'required|string',
            'certification' => 'required|string',
            'highest_degree' => 'required|string|max:255',
            'function' => 'required|string|max:255',
            'position' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ])->setAttributeNames(['fname' => 'First Name','lname' => 'Last Name']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if($data['certification']=='other'){
            $certification=$data['other'];
        }else{
            $certification=$data['certification'];
        }

        return User::create([
            'name' => $data['fname'].' '.$data['lname'],
            'email' => $data['email'],
            'industry' => $data['industry'],
            'function' => $data['function'],
            'gender' => $data['gender'],
            'position' => $data['position'],
            'country' => $data['country'],
            'years_of_experience' => $data['years_of_experience'],
            'highest_degree' => $data['highest_degree'],
            'certification' => $certification,
            'company' => $data['company'],
            'password' => bcrypt($data['password']),

        ]);

    }
}
