<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;

class DashBoardController extends Controller
{
    public function dashBoard(){
        $registerToday=DB::table('users')->select(DB::raw('*'))
            ->whereRaw('Date(created_at) = CURDATE()')->get();
        $allUsers=DB::table('users')->select(DB::raw('*'))->get();
        $passUsers=DB::table('users')->Join('results','user_id','users.id')->groupBy('user_id')->where('result','>=','80')->get();
        $allUsersResult = DB::table('main_categories')->leftJoin('results', function ($leftJoin) {
            $leftJoin->on('main_categories.id', '=', 'results.main_cat_id');


        })->select(DB::raw('avg(results.result) as avg'))->groupBy('main_categories.id')->get();
        $data = [];
        foreach ($allUsersResult as $result) {

            $data[] = $result->avg ? (float)$result->avg : 0;
        }
        $allUsersResult=$data;
        return view('admin.dashboard',compact('registerToday','allUsers','passUsers','allUsersResult'));
    }
}
