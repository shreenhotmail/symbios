<?php

namespace App\Providers;
use View;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Category;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $cats=Category::all();
        $mainCats=DB::table('main_categories')->get();
        $countries=DB::table('countries')->get();

        Schema::defaultStringLength(191);
        View::share('cats',$cats);
        View::share('mainCats',$mainCats);
        View::share('countries',$countries);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
