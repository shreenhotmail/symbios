<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    {!! Html::style ('front/css/bootstrap.min.css') !!}
    {!! Html::style ('front/css/custom.css') !!}

</head>


<body>

<div class="header">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{url('front/images/logo.png')}}">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    @if (Auth::check())
                        <li>
                            <a href="{{url('/')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                        </li>
                        <li><a href="#squarespaceModal2" data-toggle="modal"><b>Welcome, </b>{{Auth::user()->name}}</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @else
                        <li><a  href="{{ url('/login') }}">Login</a></li>
                        <li><a class="orange-btn" href="{{ url('/register') }}">Register</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>
@yield('content')

@yield('footer')
{!! Html::script ('front/js/jquery-3.1.1.min.js') !!}
{!! Html::script ('front/js/bootstrap.min.js') !!}
@yield('scripts')
<style>label {
        font-size: 20px;
    }</style>


</body>

</html>

