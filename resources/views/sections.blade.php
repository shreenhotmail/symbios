@extends('layouts.app')

@section('content')
    <style>
        .popover {
            background-color: black;
            color: #ffffff;
        }

        .popover.right > .arrow:after {
            bottom: -10px;
            left: 1px;
            content: " ";
            border-right-color: #090909;
            border-left-width: 0;
        }

        .popover-content a {
            color: #ffffff;
        }

        .fade.in {
            right: -181.656px;
        }
    </style>
    <div class="mainSection" @if(($image!='')) style="background-image:url('{{ url('images/'.$image) }}');color:black" @endif>
        <div class="container">

            <!--<div class="absimg">-->
            <!--<img src="images/advanced.png" class="img-responsive">-->
            <!--</div>-->
            @if(!empty($sections))
                <h3 class="h3-section" style="color: #286090">{{$key}} </h3>
            @endif
            <div class="main_container">
                @if(!empty($sections))

                    <?php $k = $i = 1;  ?>
                    @foreach($sections as $section)
                        @if($i%4 == 1)
                            <div class="row" align="">
                                <?php $m = 0;?>
                                @endif
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <a class=" popovers" data-toggle="popover"
                                       <?php
                                               $slag=str_replace(' ', '_',$section->cat_name);
                                               $slag=str_replace('/', '-',$slag);
                                               ?>
                                       data-content=" <a href='{{url('/questions/'.$slag)}}' title=''>{{$section->cat_name}}</a>"
                                       data-content="{{$section->cat_name}}">
                                        <div class="section">
                                            <span>1.{{$k++}}</span>
                                        </div>
                                    </a>
                                </div>

                                @if(!$loop->last)
                                    @if($m!=3)
                                        <div class="col-md-1 col-sm-1 col-xs-1">
                                            <img src="{{url('front/images/sectionimgl.png')}}" class="imgLeft">
                                        </div>

                                    @endif
                                @endif


                                @if($i%4 == 0)
                            </div>
                            <div class="row" style="margin-top: 50px">
                                <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-9 col-sm-offset-9 col-xs-offset-9">
                                </div>
                            </div>

                        @endif
                        <?php $m++;?>
                        <?php $i++; ?>
                    @endforeach

                @else
                    <div class="col-xs-12" align="center" style="margin-top: 100px; color: #ffffff"><h2>No Data Available for {{$key}}</h2></div>
                @endif
            </div>

        </div>

    </div>
@endsection
@section('scripts')
    <script>
        // popover demo
        $("[data-toggle=popover]")
                .popover({html: true})
    </script>

@endsection
