@extends('layouts.app')
{{--{{dd(Sessi{{}}on::get('allResult'))}}--}}
{{--{{dd(Session::get('data'))}}--}}
@section('content')
    <style>
        .highcharts-credits{
            display: none;
        }
    </style>
    <div class="mainResult">
        <div class="container">
            @if(Session::has('data'))
                <div class="results">
                    <div class="info">
                        <p>Thanks you</p>

                        <p>your result is {{ Session::get('result') }} % in  {{Session::get('slag')}}</p>
                    </div>
                </div>

                <div class="result-main">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="results">
                                <div class="result">
                                    <p class="mainp">You are recommended to improve your skills in the following
                                        fields </p>

                                    @if(Auth::user()->years_of_experience=='0-2')
                                        <p><span> - </span><span>Data Analytics</span></p>
                                        <p><span> - </span><span>Projects Management</span></p>
                                    @elseif(Auth::user()->years_of_experience=='2-5')
                                        <p><span> - </span><span>Finance</span></p>
                                        <p><span> - </span><span>Data Analytics</span></p>
                                        <p><span> - </span><span>Projects Management</span></p>
                                        <p><span> - </span><span>Leadership, Coaching & Influencing Skills</span></p>

                                    @elseif(Auth::user()->years_of_experience=='5-8')
                                        <p><span> - </span><span>Finance</span></p>
                                        <p><span> - </span><span>Sales & Marketing</span></p>
                                        <p><span> - </span><span>Data Analytics</span></p>
                                        <p><span> - </span><span>Projects Management</span></p>
                                        <p><span> - </span><span>Leadership, Coaching & Influencing Skills</span></p>

                                    @elseif(Auth::user()->years_of_experience=='8-13' || Auth::user()->years_of_experience=='13 - 20' || Auth::user()->years_of_experience=='20 - 30' || Auth::user()->years_of_experience=='30 - Above')
                                        <p><span> - </span><span>Strategic Management Business Planning</span></p>
                                        <p><span> - </span><span>Finance</span></p>
                                        <p><span> - </span><span>Sales & Marketing</span></p>
                                        <p><span> - </span><span>Data Analytics</span></p>
                                        <p><span> - </span><span>Projects Management</span></p>
                                        <p><span> - </span><span>Leadership, Coaching & Influencing Skills</span></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="container" style="width: 100% ; height: 400px; margin: 0 auto"></div>

                </div>
        </div>


    </div>



    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/chartist.min.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script>

        var data = {
            series: [4, 2, 2, 1, 1]
        };

        var sum = function (a, b) {
            return a + b
        };

        new Chartist.Pie('.ct-chart', data, {
            labelInterpolationFnc: function (value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
            }
        });

    </script>




    <?php $data = [];
    //var_dump($alldata);
    $alldata = array_values(Session::get('data'));
    // var_dump($alldata);
    $alldata = json_encode($alldata);
    $cat="";
    foreach($mainCats as $maincat){
        $cat.="'".$maincat->category_name ."',";
    }
    echo "<script>


        var colors = ['#E80C7A', '#000000', '#FF0000', '#E80C7A', '#E80C7A'];

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },

            title: {
                text: 'Sections Result'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [".$cat."],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Result Percentage'
                }
            },
            tooltip: {
                headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
                pointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.categories}</td>' +
                '<td style=\"padding:0\"><b>{point.y:.1f}% </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Sections Name',

                data:" . $alldata . ",

                colorByPoint: true
//colors:colors
            }]
        });

    </script>" ?>;
    @else
        <h2 align="center">Thanks For Test please select another Section </h2>
    @endif

@endsection
