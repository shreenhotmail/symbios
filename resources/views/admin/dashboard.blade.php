@extends('admin.layouts.app')
@section('head')
    {!! Html::style ('controlpanel/plugins/datatables/dataTables.bootstrap.css') !!}
    
@endsection
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3>{{count($registerToday)}}</h3>

                            <p>New Register Users Today </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        {{--<a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>--}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>{{count($passUsers)}}<sup style="font-size: 20px"></sup></h3>

                            <p>pass Users</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        {{--<a href="#" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>--}}
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-xs-4">
                    <!-- small box -->
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>{{count($allUsers)}}</h3>

                            <p>User Registrations</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        {{--<a href="#" class="small-box-footer"></a>--}}
                    </div>
                </div>
                <!-- ./col -->

                <!-- ./col -->
            </div>
            <div>
                <input class="form-control" type="text" id="biIframe"/>
                <button class="btn btn-success" style="margin-top:10px" onclick="generateReport()">send</button>
                <div id="biReport" style="margin-top:10px">
                </div>
            </div>
            <div class="row">
                <div id="container" style="width: 100% ; height: 400px; margin: 0 auto"></div>

            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    @endsection
@section('footer')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    
    <script>
        function generateReport(){
            var biIframe = document.getElementById('biIframe').value;
            document.getElementById('biReport').innerHTML = biIframe;
        }
    </script>

    <script>

        var data = {
            series: [4, 2, 2, 1, 1]
        };

        var sum = function(a, b) { return a + b };

        new Chartist.Pie('.ct-chart', data, {
            labelInterpolationFnc: function(value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
            }
        });

    </script>
    <?php
    $data=[];
    $alldata=array_values($allUsersResult);
        $alldata=json_encode($alldata);
    $cat="";
    foreach($mainCats as $maincat){
        $cat.="'".$maincat->category_name ."',";
    }
    echo "<script>


        var colors = ['#E80C7A', '#000000', '#FF0000', '#E80C7A', '#E80C7A'];

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },

            title: {
                text: 'Sections Result'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories:[ ".$cat."],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Symbios'
                }
            },
            tooltip: {
                headerFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',
                pointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.categories} </td>' +
                '<td style=\"padding:0\"><b>{point.y:.1f} % </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Sections Name',

                data:".$alldata.",

                colorByPoint: true
//colors:colors
            }]
        });

    </script>" ?>;
@endsection
