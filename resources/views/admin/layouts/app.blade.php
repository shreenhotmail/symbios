<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  @yield('head')
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Symbios | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   {!! Html::style ('admin/bootstrap/css/bootstrap.min.css') !!}
   {!! Html::style ('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
   {!! Html::style ('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') !!}
   {!! Html::style ('admin/dist/css/AdminLTE.min.css') !!}
   {!! Html::style ('admin/dist/css/skins/_all-skins.min.css') !!}
   {!! Html::style ('admin/plugins/iCheck/flat/blue.css') !!}
   {!! Html::style ('admin/plugins/morris/morris.css') !!}
   {!! Html::style ('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
   {!! Html::style ('admin/plugins/datepicker/datepicker3.css') !!}
   {!! Html::style ('admin/plugins/daterangepicker/daterangepicker.css') !!}
   {!! Html::style ('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{url('front/images/logo.png')}}" width="120px"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">


          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="{{url('controlpanel/')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>

          </a>

        </li>

        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('controlpanel/category/')}}"><i class="fa fa-circle-o"></i> Main Categories</a></li>
            <li><a href="{{url('controlpanel/subCategory/')}}"><i class="fa fa-circle-o"></i> Sub Categories</a></li>
            <li><a href="{{url('controlpanel/subCategory/create')}}"><i class="fa fa-circle-o"></i> Add New Category</a></li>
          </ul>
        </li>
          <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Questions</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('controlpanel/question/')}}"><i class="fa fa-circle-o"></i> All Questions</a></li>
            <li><a href="{{url('controlpanel/question/create')}}"><i class="fa fa-circle-o"></i> Add New Question</a></li>
          </ul>
        </li>
          <li>
              <a href="{{ route('logout') }}"
                 onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
                  Logout
              </a>

              <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          </li>

  </aside>
  <div class="content-wrapper">
  @yield('content')
</div>
<!-- jQuery UI 1.11.4 -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

 

<!-- Scripts -->
{!! Html::script ('admin/plugins/jQuery/jquery-2.2.3.min.js') !!}
{!! Html::script ('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
<script>
  // $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- {!! Html::script ('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!} -->

<!-- Bootstrap 3.3.6 -->
{!! Html::script ('admin/bootstrap/js/bootstrap.min.js') !!}
<!-- {!! Html::script ('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!} -->

{{--{!! Html::script ('admin/plugins/morris/morris.min.js') !!}--}}

<!-- FastClick -->
{!! Html::script ('admin/plugins/sparkline/jquery.sparkline.min.js') !!}

{!! Html::script ('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
{!! Html::script ('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
{!! Html::script ('admin/plugins/knob/jquery.knob.js') !!}
{!! Html::script ('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js') !!}
{!! Html::script ('admin/plugins/daterangepicker/daterangepicker.js') !!}
{!! Html::script ('admin/plugins/datepicker/bootstrap-datepicker.js') !!}
{!! Html::script ('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
{!! Html::script ('admin/plugins/slimScroll/jquery.slimscroll.min.js') !!}
{!! Html::script ('admin/plugins/fastclick/fastclick.js') !!}
{!! Html::script ('admin/dist/js/app.min.js') !!}
{!! Html::script ('admin/dist/js/pages/dashboard.js') !!}
{!! Html::script ('admin/dist/js/demo.js') !!}

@yield('footer')
    <script>
        $i=0;
        $('#addAnswer').on('click',function(){
            $i=parseInt($(".form-group").find('input[name="answerRate"]').last().val())+1;
            $('.addAnswer').append(' <div class="form-group ">\n\
            <label for="name" class="col-md-2 control-label">Answer :</label>\n\
            <div class="col-md-6">\n\
            <input type="text" class="form-control" name="answer[]" value=""  autofocus>\n\
            </div>\n\
            </div>\n\
            <div class="form-group ">\n\
            <label for="name" class="col-md-2 control-label">Rate :</label>\n\
            <div class="col-md-6">\n\
            <input type="text" class="form-control" name="answerRate[]" value="" >\n\
            </div>\n\
            </div>');
        })


    </script>
</body>
</html>
