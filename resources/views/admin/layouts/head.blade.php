<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>AdminLTE 2 | General Form Elements</title>
 <!-- Tell the browser to be responsive to screen width -->
 <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 <!-- Bootstrap 3.3.6 -->
 {!! Html::style ('controlpanel/bootstrap/css/bootstrap.min.css') !!}
 <!-- Font Awesome -->
 {!! Html::style ('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
 <!-- Ionicons -->
  {!! Html::style ('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') !!}

 <!-- Theme style -->
  {!! Html::style ('controlpanel/dist/css/AdminLTE.min.css') !!}

  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
   {!! Html::style ('controlpanel/dist/css/skins/_all-skins.min.css') !!}


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
</head>
<body>
  <div id="app">
   

  @yield('content')
</div>

<!-- Scripts -->
{!! Html::script ('controlpanel/plugins/jQuery/jquery-2.2.3.min.js') !!}

<!-- Bootstrap 3.3.6 -->
{!! Html::script ('controlpanel/bootstrap/js/bootstrap.min.js') !!}
<!-- FastClick -->
{!! Html::script ('controlpanel/plugins/fastclick/fastclick.js') !!}

<!-- AdminLTE App -->
{!! Html::script ('controlpanel/dist/js/app.min.js') !!}
{!! Html::script ('controlpanel/dist/js/demo.js') !!}

<!-- AdminLTE for demo purposes -->

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
