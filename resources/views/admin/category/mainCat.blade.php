@extends('admin.layouts.app')

@section('content')
    <div class="content" >
        <h1> Main Category </h1>
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">List main Category</div>
                        <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                      <th>Category Name</th>
                      <th>Image</th>
                      <th>#</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($mainCats as $mcat)
                      <tr>
                          <td>
                          {{$mcat->category_name}}
                          </td>
                          <td>
                            @if($mcat->image!='')  <img class="img-responsive" src="{{url('images/'.$mcat->image)}}" width="150" height="150" />@endif
                          </td>
                          <td style="width: 20%">

                              <a  class="btn btn-success" href="{{url('controlpanel/category/'.$mcat->id.'/edit')}}">
                                  <span class="glyphicon glyphicon-edit"></span>
                              </a>
                          </td>
                      </tr>
                  @endforeach

                  </tfoot>
              </table>
          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
