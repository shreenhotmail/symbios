@extends('admin.layouts.app')

@section('content')
    <div class="content" >
        <h1> Main Category </h1>
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Main Category</div>
                        <div class="panel-body" >
                            @if (Session::has('message'))
                                <div class="alert alert-info">{{ Session::get('message') }}</div>
                            @endif
                                @if(count($errors)>0)

                                    <div class="alert alert-danger print-error-msg" style="display:block">
                                        <ul>
                                            @foreach($errors as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            {{ Form::open(array('method' => 'POST','url' => array('/controlpanel/updateMain', $mainCategory->id),'class'=>'form-horizontal', 'files' => true)) }}

                            {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label">Edit ain Category</label>

                                    <div class="col-md-6">
                                    <input type="text" class="form-control" name="main_cat" value="{{ $mainCategory->category_name }}"/>

                                    </div>
                                </div>
                                <div class="form-group">
                                <label for="name" class="col-md-2 control-label">image</label>

                                    <div class="col-md-6">
                                        <input type="file" class="form-control" name="image">

                                    </div>
                                </div>

                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Category
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
