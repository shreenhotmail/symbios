@extends('admin.layouts.app')

@section('content')
    <div class="content">
        <h1> Question </h1>
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Question</div>
                        <div class="panel-body">
                            @if(count($errors) > 0)

                                <div class="alert alert-danger print-error-msg" style="display:block">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ Form::open(array('method' => 'PUT','url' => array('/controlpanel/subCategory', $cat->id),'class'=>'form-horizontal', 'files' => true)) }}

                            {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label">Main Category</label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="main_cat">
                                            @foreach($mainCats as $mcat)
                                                <option value="{{$mcat->id}}">
                                                    {{$mcat->category_name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Category</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control"value="{{$cat->cat_name}}" name="cat_name">

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Link</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" @if(!empty($cat->link))value="https://www.youtube.com/watch?v={{$cat->link}}" @endif name="link">

                                </div>
                            </div>
                                <div class="form-group">
                                <label for="name" class="col-md-2 control-label">image</label>

                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="image">

                                </div>
                            </div>

                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Category
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
