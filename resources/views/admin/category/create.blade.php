@extends('admin.layouts.app')

@section('content')
    <div class="content">
        <h1> Category </h1>

        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add New Category</div>
                        <div class="panel-body">
                               @if(count($errors) > 0)

                            <div class="alert alert-danger print-error-msg" style="display:block">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                            </div>
                            @endif

                            {{ Form::open(array('method' => 'POST','url'=>'/controlpanel/subCategory/','class'=>'form-horizontal', 'files' => true)) }}

                            {{ csrf_field() }}
                                   <div class="form-group">
                                       <label for="name" class="col-md-2 control-label">Main Category</label>

                                       <div class="col-md-6">
                                            <select class="form-control" name="main_cat">
                                                @foreach($mainCats as $cat)
                                                <option value="{{$cat->id}}">
                                                    {{$cat->category_name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                   </div>
                                   <div class="form-group">
                                       <label for="name" class="col-md-2 control-label">Category</label>

                                       <div class="col-md-6">
                                           <input type="text" class="form-control" name="cat_name">

                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label for="name" class="col-md-2 control-label">Link</label>

                                       <div class="col-md-6">
                                           <input type="text" class="form-control" name="link">

                                       </div>
                                   </div>
                                   <div class="form-group">
                                       <label for="name" class="col-md-2 control-label">Image</label>

                                       <div class="col-md-6">
                                           <input type="file" class="form-control" name="image">

                                       </div>
                                   </div>
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add Sub Category
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
