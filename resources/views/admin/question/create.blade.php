@extends('admin.layouts.app')

@section('content')
    <div class="content">
        <h1> Question </h1>

        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">Add New Question</div>
                        <div class="panel-body">
                               @if(count($errors) > 0)

                            <div class="alert alert-danger print-error-msg" style="display:block">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                            </div>
                            @endif

                            {{ Form::open(array('method' => 'POST','url'=>'/controlpanel/question/','class'=>'form-horizontal', 'files' => true)) }}

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Category</label>

                                <div class="col-md-6">
                                   <select name="cat" class="form-control">
                                       @foreach($cats as $cat)
                                           <option value="{{$cat['id']}}">{{$cat['cat_name']}}</option>
                                           @endforeach
                                   </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Question</label>

                                <div class="col-md-6">
                                    <textarea type="text" class="form-control" name="question" value=""  autofocus></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Answer A :</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="answer[]" value=""  autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Rate :</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="answerRate[]" value="" >
                                </div>
                            </div>
                            <div class="addAnswer"></div>


                            <div class="form-group">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary " id="addAnswer">Add Another Answer</button>
                                </div>
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add Question
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
