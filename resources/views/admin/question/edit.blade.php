@extends('admin.layouts.app')

@section('content')
    <div class="content">
        <h1> Question </h1>
        <div class="container-fluid">
            <div class="row">
                <div class="">
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit Question</div>
                        <div class="panel-body">
                            @if(count($errors) > 0)

                                <div class="alert alert-danger print-error-msg" style="display:block">
                                    <ul>
                                        @foreach($errors as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{ Form::open(array('method' => 'PUT','url' => array('/controlpanel/question', $question[0]->questionId),'class'=>'form-horizontal', 'files' => true)) }}

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Category</label>

                                <div class="col-md-6">
                                    <select name="cat" class="form-control">
                                        @foreach($cats as $cat)
                                            <option @if($question[0]->cat_id==$cat['id']){{'selected'}}@endif value="{{$cat['id']}}">{{$cat['cat_name']}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Question</label>

                                <div class="col-md-6">
                                    <textarea type="text" class="form-control" name="question" value=""
                                              autofocus>{{$question[0]->question}}</textarea>

                                </div>
                            </div>
                                <?php $i=0 ?>
                            @foreach($question[0]->answer as $answer)
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label">Answer A :</label>

                                    <div class="col-md-6">

                                        <input type="text" class="form-control" name="answer[]"
                                               value="{{$answer->answer}}"
                                               autofocus>
                                        <input type="hidden" class="form-control" name="answer_id[]"
                                               value="{{$answer->id}}"
                                               autofocus>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label">Rate</label>

                                    <div class="col-md-6">

                                        <input type="text" class="form-control" name="answerRate[]"
                                               value="{{$answer->rate}}"
                                               autofocus>
                                    </div>
                                </div>
                            @endforeach
                            <div class="addAnswer"></div>


                            <div class="form-group">
                                <div class="col-md-3">
                                    <button type="button" class="btn btn-primary " id="addAnswer">Add Another Answer
                                    </button>
                                </div>
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update Question
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
