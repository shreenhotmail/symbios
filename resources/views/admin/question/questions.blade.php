@extends('admin.layouts.app')
@section('head')
{!! Html::style ('controlpanel/plugins/datatables/dataTables.bootstrap.css') !!}
@endsection
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
          <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                      <th>Category Name</th>
                      <th>Question</th>
                      <th>#</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($allQuestions as $question)
                      <tr>
                          <td>
                              {{$question->cat_name}}
                          </td>
                          <td>
                              {{$question->question}}
                          </td>
                          <td style="width: 20%">
                              {{ Form::open(array('url' => 'controlpanel/question/' . $question->questionId, 'class' => 'pull-right')) }}
                              {{ Form::hidden('_method', 'DELETE') }}
                              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                              {{ Form::close() }}
                              <a  class="btn btn-success" href="{{url('/controlpanel/question/'.$question->questionId.'/edit')}}">
                                  <span class="glyphicon glyphicon-edit"></span>
                              </a>

                          </td>
                      </tr>
                  @endforeach

                  </tfoot>
              </table>
          </div>

        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->

@endsection
<style>table.dataTable thead > tr > th,td{
        text-align: center;
    }</style>
@section('footer')
{!! Html::script ('controlpanel/plugins/datatables/jquery.dataTables.min.js') !!}
{!! Html::script ('controlpanel/plugins/datatables/dataTables.bootstrap.min.js') !!}
<script>
 $('#example2').DataTable({
     "bLengthChange": false,
     "bFilter": true,
     "bInfo": false,
     "ordering": false,
     "info":     false
});
</script>

@endsection
