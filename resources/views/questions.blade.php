@extends('layouts.app')
<style>
    form {
         margin-top: 0px !important;
         margin-bottom: 0px !important;
         background-color: transparent !important;
         padding: 0px !important;
    }
</style>
@section('content')
    <div class="mainQuestion"  @if(!empty($sections->image)) style="background-image:url('{{ url('images/'.$sections->image) }}');color:black" @endif>
        <div class="container">
            <div class="text-center">
                @if(count($questions)>0)
                <h2 style="color: white">{{str_replace('-', '/',$key)}}</h2>
                    @endif
            </div>

                @if(count($questions)>0)
            <div class="questions-main">
                @if(!empty($sections->link))
                <div class="row">
                    <div class="col-xs-12" align="center">
                        <object style="padding: 10px;width: 50%; " height="300"
                                data="https://www.youtube.com/embed/{{$sections->link}}">
                        </object>
                    </div>
                </div>
                @endif
                    <div class="row">
                        {{ Form::open(array('method' => 'POST','url'=>'/questionResult/'.$key,'class'=>'form-horizontal', 'files' => true)) }}
                        {{ csrf_field() }}
                        <div class="wizard-survey" id="wizerd">
                            <?php $i = 1; $k=1;?>
                            @foreach($questions as $index=>$q)
                                <div class="step @if($i==1) progress-setp @endif " rel="{{$i}}"></div>
                                <?php $i++;?>
                            @endforeach
                        </div>
                        <div class="question_number">
                            Question Number : <span>1</span>
                        </div>
                        <div id="questions" class="col-md-12">
                            <?php $i = 1;?>
                            @foreach($questions as $index=>$q)
                                <div id="{{$i}}" class="questions" @if($i!=1)style="display: none"@endif>
                                    <div class="question">
                                        <p>{{$q->question}}</p>
                                    </div>
                                    <div class="answer">
                                        @foreach($q->answers as $answer)
                                            <p>
                                                <label><input type="radio" value="{{$answer->id}}" name="optradio_{{$q->id}}">
                                                    <span> - </span><span>{{$answer->answer}}</span>
                                                </label>
                                            </p>
                                        @endforeach


                                    </div>
                                </div>

                                <?php $i++;?>
                            @endforeach
                        </div>
                        <div class="col-md-12">
                            <div class="question_btns">
                                @if(count($questions)>1)
                                <div class="question_btn">
                                    <button class="submit-btn btn prev">
                                        Previous
                                    </button>
                                </div>
                                <div class="question_btn">
                                    <button class="submit-btn btn next">
                                        Next
                                    </button>
                                </div>
                                @endif
                                <div id="ret" question_btn class="question_btn"  @if(count($questions)===1) style="display: block;" @endif>
                                    <a href="result.html" style="color: white">
                                        <button type="submit" class="submit-btn btn">End Test</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}


                    </div>

            @else
                <div class="col-xs-12" align="center" style="margin-top: 100px; color: #ffffff"><h2>No Data Available
                        for {{$key}}</h2></div>

            @endif
            </div>
        </div>
    </div>

@endsection
@section('scripts')

    <script type="application/javascript">
        $(document).ready(function(){
            if($('#questions>div').length>1) {
                $("#questions > .questions").each(function (e) {
                    if (e != 0)
                        $(this).hide();
                });
                $("#ret, .prev").hide();
                $(".next").click(function () {
                    if ($("#questions .questions:visible").next().length != 0) {
                        $("#questions .questions:visible").next().show().prev().hide();
                        if ($("#questions .questions:visible").next().length == 0) {
                            $(".next").hide();
                            $("#ret, .prev").show();
                        } else {
                            $(".prev").show();
                        }
                    }
                    $('#questions>div').each(function () {
                        var id = $(this).index();
                        $('.question_number span').html($('#questions>div').eq(id).attr('id'));
                        if ($(this).is(':visible')) {
                            if (id == $('#questions>div').length - 1) {
                                $('#wizerd>div').eq(id).addClass('progress-setp');
                            } else {
                                $('#wizerd>div').eq(id).addClass('progress-setp');
                            }
                            return false;
                        }
                    });
                    return false;
                });

                $(".prev").click(function () {
                    if ($("#questions .questions:visible").prev().length != 0) {
                        $("#questions .questions:visible")
                                .prev()
                                .show()
                                .next()
                                .hide();
                        if ($("#questions .questions:visible").prev().length == 0) {
                            $("#ret").hide();
                            $(".next").show();
                            $(".prev").hide();
                        } else {
                            $("#ret").hide();
                            $(".next").show();
                        }
                    }
                    $('#questions>div').each(function () {
                        var id = $(this).index();
                        $('.question_number span').html($('#questions>div').eq(id).attr('id'));
                        if ($(this).is(':visible')) {
                            if (id == $('#questions>div').length - 1) {
                                $('#wizerd>div').eq(id + 1).removeClass('progress-setp');
                            } else {
                                $('#wizerd>div').eq(id + 1).removeClass('progress-setp');
                            }
                            return false;
                        }
                    });
                });
            }
        });


    </script>
@endsection
