@extends('layouts.app')

@section('content')

    <div class="container">
    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
        <div class="form_">
            <div class="row">
                <div class="co-md-12 text-center">
                    <h2>Reset Password</h2>
                </div>
                <div class="col-md-12">
                    <form  method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email address</label>
                            <input class="form-control" type="text" name="email" placeholder="Email"
                                   value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="form-group">
                            <div class="more">
                                <div class="col-md-12 text-center">
                                    <button class="submit-btn btn btn-success">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>


                </div>
            </div>
        </div>
    </div>


@endsection
