@extends('layouts.app')

@section('content')
    <div class="mainForm">
        <div class="container">
            <div class="form_ register_form">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>REGISTER</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-md-6 {{ $errors->has('fname') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> First Name   @if ($errors->has('fname'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                <input class="form-control" type="text" name="fname" value="{{old('fname')}}"
                                       placeholder="First Name">

                            </div>
                            <div class="col-md-6 {{ $errors->has('lname') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">Last Name   @if ($errors->has('lname'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                <input class="form-control" type="text" name="lname"
                                       placeholder="First Name" value="{{old('lname')}}">

                            </div>


                        </div>
                        <div class="form-group row">


                            <div class="col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Email  @if ($errors->has('email'))
                                        <span style="color: red;font-size:13px">
                                        <strong>{{$errors->first('email')}}</strong>
                                    </span>
                                    @endif</label>
                                <input class="form-control" type="email" name="email" value="{{old('email')}}"
                                       placeholder="Email">

                            </div>
                            <div class="col-md-6 {{ $errors->has('country') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Country  @if ($errors->has('country'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                               <select class="form-control" name="country">
                                   <option value="">Please select your country</option>
                                   @foreach($countries as $c)
                                       <option  @if(old('country') == "{{$c->id}}" ) selected  @endif value="{{$c->id}}"> {{$c->en_country}}</option>
                                   @endforeach
                               </select>

                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-md-6 {{ $errors->has('industry') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">Industry   @if ($errors->has('industry'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                <input class="form-control" type="text" value="{{old('industry')}}" name="industry"
                                       placeholder="Industry Name">

                            </div>
                            <div class="col-md-6 {{ $errors->has('highest_degree') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">
                                    Highest degree earned  @if ($errors->has('highest_degree'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                <select class="form-control" name="highest_degree">
                                    <option>
                                        Please Select Highest degree earned
                                    </option>
                                    <option value="High school"  @if(old('highest_degree') == "High school" ) selected  @endif >
                                        High school
                                    </option>
                                    <option value="Diploma" @if(old('highest_degree') == "Diploma" ) selected  @endif>
                                        Diploma
                                    </option>
                                    <option value="Bachelor’s in progress" @if(old('highest_degree') == "Bachelor’s in progress" ) selected  @endif>
                                        Bachelor’s in progress
                                    </option>
                                    <option value="Bachelor’s" @if(old('highest_degree') == "Bachelor’s" ) selected  @endif>
                                        Bachelor’s
                                    </option>
                                    <option value="Master’s in Progress" @if(old('highest_degree') == "Master’s in Progress" ) selected  @endif>
                                        Master’s in Progress
                                    </option>
                                    <option value="Master’s" @if(old('highest_degree') == "Master’s" ) selected  @endif>
                                        Master’s
                                    </option>
                                    <option value="Doctorate" @if(old('highest_degree') == "Doctorate" ) selected  @endif>
                                        Doctorate
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 {{ $errors->has('years_of_experience') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">Years Of Experience   @if ($errors->has('years_of_experience'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>

                                <select class="form-control" name="years_of_experience">
                                    <option>
                                        Please Select Years of Experience
                                    </option>


                                    <option value="0-2" @if(old('years_of_experience') == "0-2" ) selected  @endif>
                                        0-2
                                    </option>
                                    <option value="2-5" @if(old('years_of_experience') == "2-5" ) selected  @endif>
                                        2-5
                                    </option>
                                    <option value="5-8" @if(old('years_of_experience') == "5-8" ) selected  @endif>
                                        5-8
                                    </option>
                                    <option value="8-13" @if(old('years_of_experience') == "8-13" ) selected  @endif>
                                        8-13
                                    </option>
                                    <option value="13 - 20" @if(old('years_of_experience') == "13 - 20" ) selected  @endif>
                                        13 - 20
                                    </option>
                                    <option value="20- 30" @if(old('years_of_experience') == "20- 30" ) selected  @endif>
                                        20- 30
                                    </option>
                                    <option value="30- Above" @if(old('years_of_experience') == "30- Above" ) selected  @endif>
                                        30- Above
                                    </option>
                                </select>

                            </div>
                            <div class="col-md-6 {{ $errors->has('position') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">Position Levels   @if ($errors->has('position'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>

                                <select class="form-control" name="position">
                                    <option>
                                        Please Select position level
                                    </option>
                                    <option value="Junior" @if(old('position') == "Junior" ) selected  @endif>
                                        Junior
                                    </option>
                                    <option value="Senior" @if(old('position') == "Senior" ) selected  @endif>
                                        Senior
                                    </option>
                                    <option value="Supervisor" @if(old('position') == "Supervisor" ) selected  @endif>
                                        Supervisor
                                    </option>
                                    <option value="Manager" @if(old('position') == "Manager" ) selected  @endif>
                                        Manager
                                    </option>
                                    <option value="Director" @if(old('position') == "Director" ) selected  @endif>
                                        Director
                                    </option>
                                </select>

                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 {{ $errors->has('function') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Function  @if ($errors->has('function'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                       
                               <select class="form-control" name="function">
                                    <option>
                                        Please Select Function
                                    </option>
                                    <option value="Demand Planning" @if(old('function') == "Demand Planning" ) selected  @endif>
                                        Demand Planning
                                    </option>
                                    <option value="Supply Planning"  @if(old('function') == "Supply Planning" ) selected  @endif>
                                        Supply Planning
                                    </option>
                                    <option value="Production Planning"  @if(old('function') == "Production Planning" ) selected  @endif>
                                        Production Planning
                                    </option>
                                    <option value="Purchasing"  @if(old('function') == "Purchasing" ) selected  @endif>
                                        Purchasing
                                    </option>
                                    <option value="Logistics"  @if(old('function') == "Logistics" ) selected  @endif>
                                        Logistics
                                    </option>
                                    <option value="Inventory Management"  @if(old('function') == "Inventory Management" ) selected  @endif>
                                        Inventory Management
                                    </option>
                                    <option value="Warehousing"  @if(old('function') == "Warehousing" ) selected  @endif>
                                        Warehousing
                                    </option>
                                    <option value="Others"  @if(old('function') == "Others" ) selected  @endif>
                                        Others
                                    </option>
                                </select>        
                            </div>
                            <div class="col-md-6 {{ $errors->has('company') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Company </label>
                                <input class="form-control" type="text" name="company"
                                       placeholder="Company Name" value="{{old('company')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 {{ $errors->has('certification') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Professional certifications: 
                                    @if ($errors->has('certification'))
                                        <span style="color: red;font-size:13px">
                                        <strong>Essential *</strong>
                                    </span>
                                    @endif</label>
                                </label>
                                <select class="form-control" name="certification" onchange="check(this);">
                                    <option value="">
                                        Please Select Professional certificates
                                    </option>
                                    <option value="APICS Certified in Production & Inventory Managedment">
                                        APICS Certified in Production & Inventory Managedment
                                    </option>
                                    <option value="APICS Basics of Supply Chain Management">
                                        APICS Basics of Supply Chain Management
                                    </option>
                                    <option value="APICS Certified Supply Chain Professional">
                                        APICS Certified Supply Chain Professional
                                    </option>
                                    <option value="APICS Certified in Logistics, Transportation & Distribution">
                                        APICS Certified in Logistics, Transportation & Distribution
                                    </option>
                                    <option value="Chartered Iinstitute for Purchasing & Supply Certificate">
                                        Chartered Iinstitute for Purchasing & Supply Certificate
                                    </option>
                                    <option value="Chartered Iinstitute for Purchasing & Supply Advanced Certificate">
                                        Chartered Iinstitute for Purchasing & Supply Advanced Certificate
                                    </option>
                                    <option value="Chartered Iinstitute for Purchasing & Supply Diploma">
                                        Chartered Iinstitute for Purchasing & Supply Diploma
                                    </option>
                                    <option value="Chartered Iinstitute for Purchasing & Supply Advanced Diploma">
                                        Chartered Iinstitute for Purchasing & Supply Advanced Diploma
                                    </option>
                                    <option value="Chartered Iinstitute for Purchasing & Supply Master">
                                        Chartered Iinstitute for Purchasing & Supply Master
                                    </option>
                                    <option value="FIATA Logistics Diploma">
                                        FIATA Logistics Diploma
                                    </option>
                                    <option value="FIATA Supply Chain Diploma">
                                        FIATA Supply Chain Diploma
                                    </option>
                                    <option value="IBF Certified Professional Forecaster">
                                        IBF Certified Professional Forecaster
                                    </option>
                                    <option value="IBF Advanced - Certified Professional Forecaster">
                                        IBF Advanced - Certified Professional Forecaster
                                    </option>
                                    
                                    <option value="IPSCMI Certified International Supply Chain Manager">
                                        IPSCMI Certified International Supply Chain Manager
                                    </option>
                                    <option value="IPSCMI Certified International Purchasing Manager">
                                        IPSCMI Certified International Purchasing Manager
                                    </option>
                                    <option value="IPSCMI Certified International Supply Chain Professional">
                                        IPSCMI Certified International Supply Chain Professional
                                    </option>
                                    <option value="IPSCMI Certified International Commercial Contracts Manager">
                                        IPSCMI Certified International Commercial Contracts Manager
                                    </option>
                                    <option value="IPSCMI Certified Manager of Production & Operations">
                                        IPSCMI Certified Manager of Production & Operations
                                    </option>
                                    <option value="IPSCMI Certified International Professional Negotiator">
                                        IPSCMI Certified International Professional Negotiator
                                    </option><option value="IPSCMI Certified International Trade, Shipping & Logistics Professional">
                                        IPSCMI Certified International Trade, Shipping & Logistics Professional
                                    </option>
                                    </option><option value="ISM Certfied Professional in Supply Management">
                                        ISM Certfied Professional in Supply Management
                                    </option><option value="IACCM Contracting & Contract management International Certification">
                                        IACCM Contracting & Contract management International Certification
                                    </option>
                                    <option value="Lean Six sigma green belt">
                                        Lean Six sigma green belt

                                    </option>
                                    <option value="Lean Six sigma yellow belt">
                                        Lean Six sigma yellow belt
                                    </option>
                                    <option value="Lean Six sigma black belt">
                                        Lean Six sigma black belt

                                    </option> <option value="Lean Six sigma master black belt">
                                        Lean Six sigma master black belt
                                    </option> <option value="Lean certificates">
                                        Lean certificates
                                    </option> 
                                    <option value="MIT Micro-Master in SCM">
                                        MIT Micro-Master in SCM
                                    </option>

                                    
                                    <option value="Master in International Trade & Transport">
                                        Master in International Trade & Transport
                                    </option>
                                     <option value="PMP">
                                        PMP
                                    </option>
                                    <option value="Supply chain certificate">
                                        Supply chain certificate
                                    </option>
                                   
                                    
                                    <option value="SCC Supply Chain Operations Reference - Professional">
                                        SCC Supply Chain Operations Reference - Professional
                                    </option>
                                    
                                    
                                    <option value="other">
                                        OTHERS
                                    </option>
                                </select>
                                <br>
                                <input class="form-control" type="text" id="other" style="display: none" name="other"
                                       placeholder="Certification Name">
                                {{--@if ($errors->has('certification'))--}}
                                    {{--<span style="color: red;font-size:13px">--}}
                                        {{--<strong>Essential *</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            </div>
                            <div class="col-md-6 {{ $errors->has('gender') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Gender </label>
                                <select class="form-control" name="gender">
                                    <option>
                                        Please Select Gender
                                    </option>
                                    <option value="Male">
                                        Male
                                    </option>
                                    <option value="Female">
                                        Female
                                    </option>
                                </select>
                                {{--@if ($errors->has('gender'))--}}
                                    {{--<span style="color: red;font-size:13px">--}}
                                        {{--<strong>Essential *</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName"> Password  @if ($errors->has('password'))
                                        <span style="color: red;font-size:13px">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif</label>
                                <input class="form-control" type="password" name="password"
                                       placeholder="Password">

                            </div>
                            <div class="col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class=" col-form-label" for="userName">Confirm Password </label>
                                <input class="form-control" type="password" name="password_confirmation"
                                       placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="more">
                                <div class="col-md-12" al>
                                    <button class="submit-btn btn btn-success">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                </div>
            </div>
            </div>
        </div>
    </div>
    <script>
        function check(that) {
            if (that.value == "other") {
                document.getElementById("other").style.display = "block";
            } else {
                document.getElementById("other").style.display = "none";
            }
        }
    </script>
@endsection
