@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="homemain">
    <div class="container">
        <section class="home-elements">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title-h2">Supply Chain Competency Assessment Model </h2>
                </div>
            </div>
            <div class="row">
            @foreach($mainCats as $oneMainCat)
                <div class="col-lg-3">
                        <a class="scat-block-link" href="{{url('/sections/'.str_replace(' ','-',$oneMainCat->category_name))}}">
                            <div class="scat-block">
                                @if($oneMainCat->image)
                                <img src="{{url('images/'.$oneMainCat->image)}}">
                                @else
                                <img src="{{url('images/defualt.jpg')}}">
                                @endif
                                <h3>{{$oneMainCat->category_name}}</h3>
                            </div>
                        </a>
                </div>
            @endforeach
            </div>
        </section>
    </div>
    <section class="testimonials">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="title-h2 text-center">Testimonials and Endorsements</h2>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Carousel indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>   
                        <!-- Wrapper for carousel items -->
                        <div class="carousel-inner">
                            <div class="item carousel-item active">
                                <div class="img-box"><img src="{{url('front/images/example.jpg')}}" alt=""></div>
                                <p class="testimonial">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante. Idac bibendum scelerisque non non purus. Suspendisse varius nibh non aliquet.</p>
                                <p class="overview"><b>Alhasan</b>, Media Analyst</p>
                            </div>
                            <div class="item carousel-item">
                                <div class="img-box"><img src="{{url('front/images/example.jpg')}}" alt=""></div>
                                <p class="testimonial">Vestibulum quis quam ut magna consequat faucibus. Pellentesque eget nisi a mi suscipit tincidunt. Utmtc tempus dictum risus. Pellentesque viverra sagittis quam at mattis. Suspendisse potenti. Aliquam sit amet gravida nibh, facilisis gravida odio.</p>
                                <p class="overview"><b>Antonio Moreno</b>, Web Developer</p>
                            </div>
                            <div class="item carousel-item">
                                <div class="img-box"><img src="{{url('front/images/example.jpg')}}" alt=""></div>
                                <p class="testimonial">Phasellus vitae suscipit justo. Mauris pharetra feugiat ante id lacinia. Etiam faucibus mauris id tempor egestas. Duis luctus turpis at accumsan tincidunt. Phasellus risus risus, volutpat vel tellus ac, tincidunt fringilla massa. Etiam hendrerit dolor eget rutrum.</p>
                                <p class="overview"><b>Michael Holz</b>, Seo Analyst</p>
                            </div>
                        </div>
                        <!-- Carousel controls -->
                        <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <div class="container">
        <section class="team">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title-h2 text-center">Our Team Behind the Application</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="team-members">
                        <div class="team-avatar">
                            <img class="img-responsive" src="{{url('front/images/mohamedHafiz.jpg')}}"  alt="">
                            <a href="https://www.linkedin.com/in/mohammedhafez/" target="_blank">
                                <h4>Mohamed Hafiz</h4>
                                <span class="fa fa-linkedin-square"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-members">
                        <div class="team-avatar">
                            <img class="img-responsive" src="{{url('front/images/khaled.jpg')}}" alt="">
                            <a href="https://www.linkedin.com/in/khaledabdelmohsen/?trk=pub-pbmap" target="_blank">
                                <h4>Khaled Abdelmohsen</h4>
                                <span class="fa fa-linkedin-square"></span>
                            </a>
                        </div>
                        <div class="team-desc">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-members">
                        <div class="team-avatar">
                            <img class="img-responsive" src="{{url('front/images/maged.jpg')}}" alt="">
                            <a href="https://www.linkedin.com/in/maged-galal-0a160316/" target="_blank">
                                <h4>Maged Galal</h4>
                                <span class="fa fa-linkedin-square"></span>
                            </a>
                        </div>
                        <div class="team-desc">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-members">
                        <div class="team-avatar">
                            <img class="img-responsive" src="{{url('front/images/shereen.jpg')}}" alt="">
                            <a href=" https://www.linkedin.com/in/shereen-mosallam-symbios-consulting-53506811/" target="_blank">
                                <h4>Shereen Mosallam</h4>
                                <span class="fa fa-linkedin-square"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- //end row -->

        </section>
    </div>

    <div class="homeback">
        <div class='selector'>
            <ul>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain1">
                    <input id='c2' type='checkbox'>
                    <label for='c2'>
                        <a href="{{url('/sections/Basic-Concepts-of-Supply-Chain')}}">
                            <p class="ctext">Basic Concepts of Supply Chain
                            </p>
                            <img src="{{url('front/images/Basics%20Concept%20of%20supply%20Chain.jpg')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain2">
                    <input id='c5' type='checkbox'>
                    <label for='c5'>
                        <a href="{{url('/sections/Planning-Process')}}">
                            <p class="ctext ctx4">Planning Process</p>
                            <img src="{{url('front/images/Planning%20Concept.jpg')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain3">
                    <input id='c8' type='checkbox'>
                    <label for='c8'>
                        <a href="{{url('/sections/Sourcing-and-Procurement')}}">
                            <p class="ctext">Sourcing and Procurement</p>
                            <img src="{{url('front/images/Sourcing%20Concept.png')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain4">
                    <input id='c4' type='checkbox'>
                    <label for='c4'>
                        <a href="{{url('/sections/Manufacturing-and-Executions')}}">
                            <p class="ctext ctx3">Manufacturing and Executions</p>
                            <img src="{{url('front/images/Manufacturing.jpg')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain5">
                    <input id='c3' type='checkbox'>
                    <label for='c3'>
                        <a href="{{url('/sections/Delivery-and-Logistics')}}">
                            <p class="ctext ctx2">Delivery and Logistics</p>
                            <img src="{{url('front/images/delivery.jpg')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain6">
                    <input id='c6' type='checkbox'>
                    <label for='c6'>
                        <a href="{{url('/sections/Return-and-Reverse-Logistics')}}">
                            <p class="ctext">Return and Reverse Logistics</p>
                            <img src="{{url('front/images/Return.jpg')}}">
                        </a>
                    </label>
                </li>
                <li>
                    <img src="{{url('front/images/sectionimgt.png')}}" class="imgchain8">
                    <input id='c1' type='checkbox'>
                    <label for='c1'>
                        <a href="{{url('/sections/Advanced-Supply-Chain')}}">
                            <p class="ctext">Advanced Supply Chain</p>
                            <img src="{{url('front/images/advanced.png')}}">
                        </a>
                    </label>
                </li>






            </ul>
            <button>
                <b>SCAT</b>
                <br/>
                Supply Chain Competency Assessment Module <span class="glyphicon glyphicon-registration-mark"></span>

            </button>
        </div>
    </div>
</div>
</div>

@endsection
@section('footer')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>var nbOptions = 8;
var angleStart = -360;

        // jquery rotate animation
        function rotate(li,d) {
            $({d:angleStart}).animate({d:d}, {
                step: function(now) {
                    $(li)
                    .css({ transform: 'rotate('+now+'deg)' })
                    .find('label')
                    .css({ transform: 'rotate('+(-now)+'deg)' });
                    $('.imgchain1 ,.imgchain2 ,.imgchain3 ,.imgchain4 ,.imgchain5 ,.imgchain6 ,.imgchain7 ,.imgchain8').fadeIn(6000);
                    $('.imgchain8').css({
                        'width': '100px',
                        'margin-top': '0px',
                        'margin-left': '-50px',
                        'opacity': '1',
                        WebkitTransition : 'opacity 6s ease-in-out',
                        MozTransition    : 'opacity 6s ease-in-out',
                        MsTransition     : 'opacity 6s ease-in-out',
                        OTransition      : 'opacity 6s ease-in-out',
                        transition       : 'opacity 6s ease-in-out'
                    });
                }, duration: 0
            });
        }

        // show / hide the options
        function toggleOptions(s) {
            $(s).toggleClass('open');
            var li = $(s).find('li');
            var deg = $(s).hasClass('half') ? 180/(li.length-1) : 360/li.length;
            for(var i=0; i<li.length; i++) {
                var d = $(s).hasClass('half') ? (i*deg)-90 : i*deg;
                $(s).hasClass('open') ? rotate(li[i],d) : rotate(li[i],angleStart);
            }
        }
        setTimeout(function() { toggleOptions('.selector'); }, 100);//@ sourceURL=pen.js
    </script>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    @endsection
