-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2019 at 01:11 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `symbios`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `answer` varchar(191) NOT NULL,
  `is_correct` tinyint(4) NOT NULL,
  `rate` int(10) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer`, `is_correct`, `rate`, `question_id`, `created_at`, `updated_at`) VALUES
(1, '1', 0, 0, 1, '2019-07-13 08:16:00', '2019-07-13 08:16:40'),
(2, '2', 0, 0, 1, '2019-07-13 08:16:00', '2019-07-13 08:16:40'),
(3, '3', 1, 0, 1, '2019-07-13 08:16:00', '2019-07-13 08:16:40'),
(4, '4', 0, 0, 1, '2019-07-13 08:16:00', '2019-07-13 08:16:40'),
(5, '5', 0, 0, 1, '2019-07-13 08:16:01', '2019-07-13 08:16:41'),
(6, '1', 1, 0, 2, '2019-07-13 08:18:14', '2019-07-13 08:18:14'),
(7, '2', 0, 0, 2, '2019-07-13 08:18:14', '2019-07-13 08:18:14'),
(8, '3', 0, 0, 2, '2019-07-13 08:18:15', '2019-07-13 08:18:15'),
(9, '4', 0, 0, 2, '2019-07-13 08:18:15', '2019-07-13 08:18:15'),
(10, '5', 0, 0, 2, '2019-07-13 08:18:15', '2019-07-13 08:18:15'),
(11, '1', 1, 100, 3, '2019-07-21 16:11:38', '2019-08-04 11:23:15'),
(12, '2', 0, 0, 3, '2019-07-21 16:11:38', '2019-08-04 11:23:15'),
(13, 'one', 1, 20, 4, '2019-07-21 16:16:06', '2019-07-21 16:16:06'),
(14, 'two', 0, 0, 4, '2019-07-21 16:16:06', '2019-07-21 16:16:06'),
(15, 'three', 1, 0, 5, '2019-07-21 16:24:29', '2019-07-21 16:24:29'),
(16, 'four', 0, 0, 5, '2019-07-21 16:24:29', '2019-07-21 16:24:29'),
(17, '1', 0, 0, 7, '2019-08-01 09:15:31', '2019-08-01 09:15:31'),
(18, '2', 0, 0, 7, '2019-08-01 09:15:31', '2019-08-01 09:15:31'),
(19, '1', 0, 0, 8, '2019-08-01 09:21:37', '2019-08-01 09:21:37'),
(20, '2', 0, 0, 8, '2019-08-01 09:21:37', '2019-08-01 09:21:37'),
(21, '3', 0, 0, 8, '2019-08-01 09:21:37', '2019-08-01 09:21:37'),
(22, '1', 0, 10, 9, '2019-08-01 09:23:54', '2019-08-04 11:19:18'),
(23, '2', 0, 20, 9, '2019-08-01 09:23:54', '2019-08-04 11:19:18'),
(24, '3', 0, 30, 9, '2019-08-04 11:19:19', '2019-08-04 11:19:19'),
(25, 'subcat10', 0, 10, 10, '2019-08-04 17:00:29', '2019-08-04 17:00:29'),
(26, 'subcat20', 0, 20, 10, '2019-08-04 17:00:29', '2019-08-04 17:00:29'),
(27, 'subcatt70', 0, 70, 10, '2019-08-04 17:00:29', '2019-08-04 17:00:29'),
(28, 'subcat210', 0, 10, 11, '2019-08-04 17:02:14', '2019-08-04 17:02:14'),
(29, 'subcat250', 0, 50, 11, '2019-08-04 17:02:14', '2019-08-04 17:02:14'),
(30, '1', 0, 20, 12, '2019-08-04 17:04:23', '2019-08-04 17:04:23'),
(31, '2', 0, 40, 12, '2019-08-04 17:04:23', '2019-08-04 17:04:23'),
(32, '3', 0, 40, 12, '2019-08-04 17:04:23', '2019-08-04 17:04:23'),
(33, '1', 0, 50, 13, '2019-08-04 17:05:34', '2019-08-04 17:05:34'),
(34, '2', 0, 40, 13, '2019-08-04 17:05:34', '2019-08-04 17:05:34'),
(35, '3', 0, 10, 13, '2019-08-04 17:05:35', '2019-08-04 17:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) NOT NULL,
  `link` varchar(350) NOT NULL,
  `image` varchar(350) NOT NULL,
  `main_cat` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `link`, `image`, `main_cat`, `created_at`, `updated_at`) VALUES
(1, 'cat_name_2', '', '', 1, '2019-06-26 22:00:00', NULL),
(2, 'New path to purchase', '', '', 9, '2019-07-13 08:14:14', '2019-07-13 08:14:14'),
(3, 'test', '', '', 7, '2019-07-21 16:15:07', '2019-07-21 16:15:07'),
(4, 'test3', '', '', 1, '2019-07-21 16:23:40', '2019-08-01 08:12:36'),
(5, 'shreen', '', '1564926516.png', 1, '2019-08-04 11:47:36', '2019-08-04 11:48:36'),
(6, 'Return subcat1', '', '', 2, '2019-08-04 16:57:07', '2019-08-04 16:57:07'),
(7, 'Return subcat2', '', '', 2, '2019-08-04 16:58:07', '2019-08-04 16:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `en_country` varchar(200) NOT NULL,
  `code` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `en_country`, `code`) VALUES
(233, 'Afghanistan', 'AF'),
(2, 'land Islands', 'AX'),
(3, 'Albania', 'AL'),
(4, 'Algeria', 'DZ'),
(5, 'American Samoa', 'AS'),
(6, 'Andorra', 'AD'),
(7, 'Angola', 'AO'),
(8, 'Anguilla', 'AI'),
(9, 'Antarctica', 'AQ'),
(10, 'Antigua and Barbuda', 'AG'),
(11, 'Argentina', 'AR'),
(12, 'Armenia', 'AM'),
(13, 'Aruba', 'AW'),
(14, 'Australia', 'AU'),
(15, 'Austria', 'AT'),
(16, 'Azerbaijan', 'AZ'),
(17, 'Bahamas', 'BS'),
(18, 'Bahrain', 'BH'),
(19, 'Bangladesh', 'BD'),
(20, 'Barbados', 'BB'),
(21, 'Belarus', 'BY'),
(22, 'Belgium', 'BE'),
(23, 'Belize', 'BZ'),
(24, 'Benin', 'BJ'),
(25, 'Bermuda', 'BM'),
(26, 'Bhutan', 'BT'),
(27, 'Bosnia and Herzegovina', 'BA'),
(28, 'Botswana', 'BW'),
(29, 'Bouvet Island', 'BV'),
(30, 'Brazil', 'BR'),
(31, 'British Indian Ocean Territory', 'IO'),
(32, 'Brunei Darussalam', 'BN'),
(33, 'Bulgaria', 'BG'),
(34, 'Burkina Faso', 'BF'),
(35, 'Burundi', 'BI'),
(36, 'Cambodia', 'KH'),
(37, 'Cameroon', 'CM'),
(38, 'Canada', 'CA'),
(39, 'Cape Verde', 'CV'),
(40, 'Cayman Islands', 'KY'),
(41, 'Central African Republic', 'CF'),
(42, 'Chad', 'TD'),
(43, 'Chile', 'CL'),
(44, 'China', 'CN'),
(45, 'Christmas Island', 'CX'),
(46, 'Cocos (Keeling) Islands', 'CC'),
(47, 'Colombia', 'CO'),
(48, 'Comoros', 'KM'),
(49, 'Congo', 'CG'),
(50, 'Cook Islands', 'CK'),
(51, 'Costa Rica', 'CR'),
(52, 'C?te d\'Ivoire', 'CI'),
(53, 'Croatia', 'HR'),
(54, 'Cuba', 'CU'),
(55, 'Cura?ao', 'CW'),
(56, 'Cyprus', 'CY'),
(57, 'Czech Republic', 'CZ'),
(58, 'Denmark', 'DK'),
(59, 'Djibouti', 'DJ'),
(60, 'Dominica', 'DM'),
(61, 'Dominican Republic', 'DO'),
(62, 'Ecuador', 'EC'),
(1, 'Egypt', 'EG'),
(64, 'El Salvador', 'SV'),
(65, 'Equatorial Guinea', 'GQ'),
(66, 'Eritrea', 'ER'),
(67, 'Estonia', 'EE'),
(68, 'Ethiopia', 'ET'),
(69, 'Falkland Islands (Malvinas)', 'FK'),
(70, 'Faroe Islands', 'FO'),
(71, 'Fiji', 'FJ'),
(72, 'Finland', 'FI'),
(73, 'France', 'FR'),
(74, 'French Guiana', 'GF'),
(75, 'French Polynesia', 'PF'),
(76, 'French Southern Territories', 'TF'),
(77, 'Gabon', 'GA'),
(78, 'Gambia', 'GM'),
(79, 'Georgia', 'GE'),
(80, 'Germany', 'DE'),
(81, 'Ghana', 'GH'),
(82, 'Gibraltar', 'GI'),
(83, 'Greece', 'GR'),
(84, 'Greenland', 'GL'),
(85, 'Grenada', 'GD'),
(86, 'Guadeloupe', 'GP'),
(87, 'Guam', 'GU'),
(88, 'Guatemala', 'GT'),
(89, 'Guernsey', 'GG'),
(90, 'Guinea', 'GN'),
(91, 'Guinea-Bissau', 'GW'),
(92, 'Guyana', 'GY'),
(93, 'Haiti', 'HT'),
(94, 'Heard Island and McDonald Islands', 'HM'),
(95, 'Holy See (Vatican City State)', 'VA'),
(96, 'Honduras', 'HN'),
(97, 'Hong Kong', 'HK'),
(98, 'Hungary', 'HU'),
(99, 'Iceland', 'IS'),
(100, 'India', 'IN'),
(101, 'Indonesia', 'ID'),
(102, 'Iraq', 'IQ'),
(103, 'Ireland', 'IE'),
(104, 'Isle of Man', 'IM'),
(105, 'Israel', 'IL'),
(106, 'Italy', 'IT'),
(107, 'Jamaica', 'JM'),
(108, 'Japan', 'JP'),
(109, 'Jersey', 'JE'),
(110, 'Jordan', 'JO'),
(111, 'Kazakhstan', 'KZ'),
(112, 'Kenya', 'KE'),
(113, 'Kiribati', 'KI'),
(114, 'Kuwait', 'KW'),
(115, 'Kyrgyzstan', 'KG'),
(116, 'Latvia', 'LV'),
(117, 'Lebanon', 'LB'),
(118, 'Lesotho', 'LS'),
(119, 'Liberia', 'LR'),
(120, 'Libya', 'LY'),
(121, 'Liechtenstein', 'LI'),
(122, 'Lithuania', 'LT'),
(123, 'Luxembourg', 'LU'),
(124, 'Madagascar', 'MG'),
(125, 'Malawi', 'MW'),
(126, 'Malaysia', 'MY'),
(127, 'Maldives', 'MV'),
(128, 'Mali', 'ML'),
(129, 'Malta', 'MT'),
(130, 'Marshall Islands', 'MH'),
(131, 'Martinique', 'MQ'),
(132, 'Mauritania', 'MR'),
(133, 'Mauritius', 'MU'),
(134, 'Mayotte', 'YT'),
(135, 'Mexico', 'MX'),
(136, 'Monaco', 'MC'),
(137, 'Mongolia', 'MN'),
(138, 'Montenegro', 'ME'),
(139, 'Montserrat', 'MS'),
(140, 'Morocco', 'MA'),
(141, 'Mozambique', 'MZ'),
(142, 'Myanmar', 'MM'),
(143, 'Namibia', 'NA'),
(144, 'Nauru', 'NR'),
(145, 'Nepal', 'NP'),
(146, 'Netherlands', 'NL'),
(147, 'New Caledonia', 'NC'),
(148, 'New Zealand', 'NZ'),
(149, 'Nicaragua', 'NI'),
(150, 'Niger', 'NE'),
(151, 'Nigeria', 'NG'),
(152, 'Niue', 'NU'),
(153, 'Norfolk Island', 'NF'),
(154, 'Northern Mariana Islands', 'MP'),
(155, 'Norway', 'NO'),
(156, 'Oman', 'OM'),
(157, 'Pakistan', 'PK'),
(158, 'Palau', 'PW'),
(159, 'Panama', 'PA'),
(160, 'Papua New Guinea', 'PG'),
(161, 'Paraguay', 'PY'),
(162, 'Peru', 'PE'),
(163, 'Philippines', 'PH'),
(164, 'Pitcairn', 'PN'),
(165, 'Poland', 'PL'),
(166, 'Portugal', 'PT'),
(167, 'Puerto Rico', 'PR'),
(168, 'Qatar', 'QA'),
(169, 'R?union', 'RE'),
(170, 'Romania', 'RO'),
(171, 'Russian Federation', 'RU'),
(172, 'Rwanda', 'RW'),
(173, 'Saint Barth?lemy', 'BL'),
(174, 'Saint Kitts and Nevis', 'KN'),
(175, 'Saint Lucia', 'LC'),
(176, 'Saint Martin (French part)', 'MF'),
(177, 'Saint Pierre and Miquelon', 'PM'),
(178, 'Saint Vincent and the Grenadines', 'VC'),
(179, 'Samoa', 'WS'),
(180, 'San Marino', 'SM'),
(181, 'Sao Tome and Principe', 'ST'),
(182, 'Saudi Arabia', 'SA'),
(183, 'Senegal', 'SN'),
(184, 'Serbia', 'RS'),
(185, 'Seychelles', 'SC'),
(186, 'Sierra Leone', 'SL'),
(187, 'Singapore', 'SG'),
(188, 'Sint Maarten (Dutch part)', 'SX'),
(189, 'Slovakia', 'SK'),
(190, 'Slovenia', 'SI'),
(191, 'Solomon Islands', 'SB'),
(192, 'Somalia', 'SO'),
(193, 'South Africa', 'ZA'),
(194, 'South Georgia and the South Sandwich Islands', 'GS'),
(195, 'South Sudan', 'SS'),
(196, 'Spain', 'ES'),
(197, 'Sri Lanka', 'LK'),
(198, 'Sudan', 'SD'),
(199, 'Suriname', 'SR'),
(200, 'Svalbard and Jan Mayen', 'SJ'),
(201, 'Swaziland', 'SZ'),
(202, 'Sweden', 'SE'),
(203, 'Switzerland', 'CH'),
(204, 'Syrian Arab Republic', 'SY'),
(205, 'Tajikistan', 'TJ'),
(206, 'Thailand', 'TH'),
(207, 'Timor-Leste', 'TL'),
(208, 'Togo', 'TG'),
(209, 'Tokelau', 'TK'),
(210, 'Tonga', 'TO'),
(211, 'Trinidad and Tobago', 'TT'),
(212, 'Tunisia', 'TN'),
(213, 'Turkey', 'TR'),
(214, 'Turkmenistan', 'TM'),
(215, 'Turks and Caicos Islands', 'TC'),
(216, 'Tuvalu', 'TV'),
(217, 'Uganda', 'UG'),
(218, 'Ukraine', 'UA'),
(219, 'United Arab Emirates', 'AE'),
(220, 'United Kingdom', 'GB'),
(221, 'United States', 'US'),
(222, 'United States Minor Outlying Islands', 'UM'),
(223, 'Uruguay', 'UY'),
(224, 'Uzbekistan', 'UZ'),
(225, 'Vanuatu', 'VU'),
(226, 'Viet Nam', 'VN'),
(227, 'Wallis and Futuna', 'WF'),
(228, 'Western Sahara', 'EH'),
(229, 'Yemen', 'YE'),
(230, 'Zambia', 'ZM'),
(231, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(350) NOT NULL,
  `image` varchar(350) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `category_name`, `image`) VALUES
(1, 'Advanced Supply Chain', '1564929118.png'),
(2, 'Return and Reverse Logistics', '1564929164.png'),
(3, 'Delivery and Logistics', '1564929193.png'),
(4, 'Manufacturing and Execution', '1564929222.png'),
(5, 'Sourcing and Procurement', '1564929501.png'),
(6, 'Planning Processes', '1564929536.png'),
(7, 'Basic Concepts of Supply Chain', '1564929566.png'),
(8, 'Digital Supply Chain', '1564929602.png'),
(9, 'Consumer centric', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_04_11_134641_create_questions_table', 1),
(8, '2018_04_14_102649_create_answers_table', 1),
(9, '2018_04_14_103557_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `question` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `cat_id`, `question`, `created_at`, `updated_at`) VALUES
(1, 2, 'To which extent digitalization of your sales force? (mobile devices, accessability to all relevant system and at client site)', '2019-07-13 08:16:00', '2019-07-13 08:16:39'),
(2, 2, 'To which extent your customer tailored at your pricing system? ', '2019-07-13 08:18:14', '2019-07-13 08:18:14'),
(3, 1, 'test question?', '2019-07-21 16:11:38', '2019-08-04 11:23:15'),
(4, 3, 'test?', '2019-07-21 16:16:06', '2019-07-21 16:16:06'),
(5, 4, 'test2?', '2019-07-21 16:24:29', '2019-07-21 16:24:29'),
(6, 4, 'test', '2019-08-01 09:12:11', '2019-08-01 09:12:11'),
(7, 1, 'test', '2019-08-01 09:15:31', '2019-08-01 09:15:31'),
(8, 1, 'taaaaa', '2019-08-01 09:21:37', '2019-08-01 09:21:37'),
(9, 3, 'yyyyy2', '2019-08-01 09:23:54', '2019-08-04 11:19:18'),
(10, 6, 'Question subcat1', '2019-08-04 17:00:29', '2019-08-04 17:00:29'),
(11, 7, 'Question subcat2', '2019-08-04 17:02:14', '2019-08-04 17:02:14'),
(12, 6, 'Question 2 subcat1', '2019-08-04 17:04:23', '2019-08-04 17:04:23'),
(13, 7, 'Q222', '2019-08-04 17:05:34', '2019-08-04 17:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `main_cat_id` int(11) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `result` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `user_id`, `main_cat_id`, `sub_cat_id`, `result`, `created_at`) VALUES
(1, 13, 7, 3, 50, '2019-07-21 18:16:31'),
(2, 13, 7, 4, 100, '2019-07-21 18:25:15'),
(3, 13, 2, 6, 50, '2019-08-04 19:07:23'),
(4, 13, 2, 7, 100, '2019-08-04 19:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci NOT NULL,
  `years_of_experience` enum('0-2','2-5','5-8','8-13','13 - Above') COLLATE utf8mb4_unicode_ci NOT NULL,
  `highest_degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `industry`, `function`, `gender`, `years_of_experience`, `highest_degree`, `certification`, `company`, `position`, `country`, `remember_token`, `admin`, `created_at`, `updated_at`) VALUES
(1, 'Ahmed hashem', 'ahmedhashem@gmail.com', '$2y$10$C.6h1nqMZi9BDKsPHpQd0OG9G3.iwcGGzsEqSQQC4jmk0JwwH/r.2', 'test', 'test', 'Male', '', 'bachelor ', 'Male', 'test', NULL, 0, 'sW1mTKS8dWch02FqYFiFk2gr3QqWJRcvggOCtBENwZ1m3vtXDSO1vClxgyhu', 1, '2018-05-06 06:35:54', '2018-05-06 06:35:54'),
(2, 'saSas ASA', 'rababahmed88@gmail.com', '$2y$10$W3Etthri4CShr0sCu4vvYOMqImOIaJ58lILhGfzyBXD75OYwiq9dS', 'asAS', 'Sa', 'Male', '', 'Sas', 'Female', 'AS', NULL, 0, 'OuR4SvxdPaDsOp2U5TcVEMMbONA5adc6cGZFe7IvCDkjr3Uv7VGvm35W9aCu', 0, '2018-05-07 11:15:39', '2018-05-07 11:15:39'),
(3, 'ahmed', ' ahmed.hashem@symbios-consulting.com', '$2y$10$ya56hfko0m/ZjR/FWMzPRO1/u7I6AEDj8rG030P/be1LO7gY9cfMe', 'ff', 'ddd', '', '', 'fff', 'Male', 'cscsc', NULL, 0, 'AUjE5KH6e3xZwivp4glejzkstRoyj82HHjT11JEMfLsPwPkl5xHwjp2W4nHN', 0, '2018-05-13 21:39:28', '2018-05-13 21:39:28'),
(4, 'Khaled Abdelmohsen El-Asi', 'Kabdelmohsen@gmail.com', '$2y$10$n5Ta3vwPfhdwzbUofLkjSeZe9mRlN5iGzCtRJhdSU/qmfj5iAN0Ty', 'Pharmaceutical', 'Planning', 'Male', '', 'Bachelor ', 'Please Select Professional certificates', 'Servier', NULL, 0, NULL, 0, '2018-05-17 21:22:08', '2018-05-17 21:22:08'),
(5, 'shereen mosallam', 'shereen@symbios-solutions.com', '$2y$10$IvH.qX6elK6tDAj4dUlrKOMtv.RlZ0PSw8E5oQpkt34QRVaQAmjee', 'consulting', 'consultant', 'Female', '', 'MBA', 'Female', 'Symbios Solutions', NULL, 0, NULL, 0, '2018-06-19 19:20:07', '2018-06-19 19:20:07'),
(6, 'malak mohamed', 'm@m.com', '$2y$10$TU7jpFZr.YUEq6IB5T15L.eZzzZHknrM2Um5TEmLM060zwLEX8LSu', 'ddd', 'csds', 'Female', '0-2', 'dd', 'Supply chain certificate', 'sds', 'Junior', 1, 'En3XIylmxucF7HrkFgL3ZLDCHUbZTu6pwaeMREM09CpYDqqteaeXlJuYZCIs', 0, '2018-07-16 03:26:11', '2018-07-16 03:26:11'),
(7, 'test sasds', 'test@gmail.com', '$2y$10$euYKZ6LnptIPYMRpie.u7ebYpmQbExU3jmJijmQsn85ZxTmNaKICq', 'sdsd', 'ssd', 'Male', '0-2', 'sdsd', 'Lean Six sigma green belt', 'scdsd', 'Junior', 19, NULL, 0, '2018-07-28 22:09:25', '2018-07-28 22:09:25'),
(8, 'Ahmed Hashem', 'ahmed.hashem@symbios-consulting.com', '$2y$10$h47DvGd.IIspgjClWaRA1uvjeMM6DJnZmayXxCxV9BBDvEWIHd/M6', 'Manufacturing ', 'Analytics', 'Male', '13 - Above', 'Bcs', 'Lean Six sigma black belt', 'Symbios-Consulting ', 'Manager', 1, '2opU3QtTKCpVpSaZ12AvG7aH2eJqIz59sXJKjkHM1R3koAoFC2RXQLCiQr0z', 0, '2018-07-29 00:22:55', '2018-08-09 20:55:16'),
(9, 'rabab Hassoaun', 'rabab12@gmail.com', '$2y$10$pf0GuoJ/9WcURonNVPyO/eTDcFkbeQobHHzF8fUumIxnwz53sbeNO', 'dsdsdsd', 'Demand Planning', 'Male', '0-2', 'High school', 'Lean Six sigma green belt', 'sdsdsds', 'Junior', 233, NULL, 0, '2018-08-07 00:41:21', '2018-08-07 00:41:21'),
(10, 'Maged Galal', 'maged.galal80@gmail.com', '$2y$10$cMnyrZnhlIpxCp7Lb5GI4O5Gk3KVIRUeEbuqovo6NTt5XRXtQZr/G', 'No thing', 'Logistics', 'Male', '', 'Master’s in Progress', 'APICS Basics of Supply Chain Management', 'No thing', 'Director', 1, NULL, 0, '2018-08-08 00:51:14', '2018-08-08 00:51:14'),
(11, 'shreen abdou', 'shreen@gmail.com', '$2y$10$C.6h1nqMZi9BDKsPHpQd0OG9G3.iwcGGzsEqSQQC4jmk0JwwH/r.2', 'it', 'Production Planning', 'Female', '2-5', 'Bachelor’s', 'FIATA Supply Chain Diploma', 'xyz', 'Senior', 1, NULL, 0, '2019-06-27 08:08:49', '2019-06-27 08:08:49'),
(12, 'shreen abdou', 'admin@gmail.com', '$2y$10$C.6h1nqMZi9BDKsPHpQd0OG9G3.iwcGGzsEqSQQC4jmk0JwwH/r.2', 'it', 'Production Planning', 'Female', '2-5', 'Bachelor’s', 'FIATA Supply Chain Diploma', 'xyz', 'Senior', 1, NULL, 1, '2019-06-27 08:08:49', '2019-06-27 08:08:49'),
(13, 'test test2', 'test@y.com', '$2y$10$K5NJKYVrI466g5W4iuA6du1D0gZ3ypsXr6uI9mIbKgsFaj7NLw0bO', 'it', 'Demand Planning', 'Female', '0-2', 'Diploma', 'FIATA Logistics Diploma', 'kk', 'Junior', 1, 'eloWnndEjK11SLuXNJZgqjktf1RaQGKHYOpRuMLCnTnayDtiC3JfjJJsB8RR', 1, '2019-07-03 08:52:07', '2019-07-03 08:52:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=637;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
